<?php

namespace Drupal\mouseflow_tracking\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class MouseflowTrackingSettingsForm.
 *
 * Admin config form definition for the scroll to top button options.
 *
 * @package Drupal\mouseflow_tracking\Form
 */
class MouseflowTrackingSettingsForm extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'mouseflow_tracking.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'mouseflow_tracking';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);

    $form['general_settings'] = [
      '#type'  => 'details',
      '#title' => $this->t('General settings'),
      '#open' => TRUE,
    ];

    $form['general_settings']['enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Mouseflow Tracking'),
      '#default_value' => $config->get('enabled'),
    ];

    $form['general_settings']['allow_admin_pages'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow Mouseflow Tracking for admin pages'),
      '#default_value' => $config->get('allow_admin_pages'),
    ];

    $form['general_settings']['tracking_code'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Copy and paste Mouseflow Tracking code'),
      '#default_value' => $config->get('tracking_code'),
      '#required' => TRUE,
      '#description' => $this->t('In your mouseflow account, You can get this 
                                  code from the website list or dashboard 
                                  (by clicking the </> icon) or under 
                                  Settings > Tracking Code once you open a 
                                  specific website. If you need more detailed 
                                  steps, please take a look at this installation
                                  <a href="https://help.mouseflow.com/en/articles/4264201-how-to-install-mouseflow" target="_blank">guide</a>.'),
    ];

    $form['other_settings'] = [
      '#type'  => 'details',
      '#title' => $this->t('Restrict Mouseflow Tracking'),
      '#open' => TRUE,
    ];

    $form['other_settings']['disabled_pages'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Pages to disallow tracking'),
      '#default_value' => $config->get('disabled_pages'),
      '#description' => $this->t("Specify pages by using their paths. Enter 
                                  one path per line. An example path is '/node/1' 
                                  for a page. <front> is the front page."),
    ];

    $form['other_settings']['blocked_ips'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Blocked IP addresses'),
      '#default_value' => $config->get('blocked_ips'),
      '#description' => $this->t('Enter one IP address per line.'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Retrieve the configuration.
    $this->config(static::SETTINGS)
      // Set the submitted configuration setting.
      ->set('enabled', $form_state->getValue('enabled'))
      ->set('allow_admin_pages', $form_state->getValue('allow_admin_pages'))
      ->set('tracking_code', $form_state->getValue('tracking_code'))
      ->set('disabled_pages', $form_state->getValue('disabled_pages'))
      ->set('blocked_ips', $form_state->getValue('blocked_ips'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
