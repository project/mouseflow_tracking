Mouseflow Tracking
====================
DESCRIPTION
-----------
  Mouseflow Tracking allows your site to be tracked by Mouseflow by adding a 
  Javascript tracking code to pages. 
  There are options to restrict tracking for admin pages, specified 
  pages, and specified IP addresses as well.

  By using this module, Mouseflow can be installed on your website in less than 
  a minute. 

CONFIGURATION
-------------
  The configuration page is at admin/config/system/mouseflow-tracking, where 
  you can configure the mouseflow tracking code and enable it to track the 
  mouseflow on your website.

  In Mouseflow account, you can retrieve the tracking code from the website 
  list or dashboard (by clicking the </> icon) or under Settings > Tracking Code
   once you open a specific website.

  If you need more detailed steps, please take a look at this installation 
  [guide](https://help.mouseflow.com/en/articles/4264201-how-to-install-mouseflow).


UNINSTALLATION
--------------
  1. Disable the module from 'admin/modules/uninstall'.
  2. Uninstall the module
